import requests
import os
import time
import threading

from requests import RequestException

MAX_THREADS = 50
THREADS_DONE_TIME_DELTA = 0.1

today = time.strftime("%Y-%m-%d", time.localtime())


def smol_time():
    time_now = time.time()
    return time.strftime(f"[%H:%M:%S.{int((time_now - int(time_now))*1000)}] ", time.localtime())


class Emoji:
    emojis = list()
    ids = list()
    names = list()

    def __init__(self, snowflake, name, t):
        self.id = snowflake
        self.name = name
        self.type = t
        Emoji.emojis.append(self)
        Emoji.ids.append(snowflake)
        Emoji.names.append(name)

    @property
    def link(self):
        return "https://cdn.discordapp.com/emojis/" + self.id + "." + self.type

    def already_downloaded(self):
        return os.path.isfile(os.path.join(folder_name, self.name + "." + self.type))


mode = str()
while mode not in ['1', '2', '3']:
    mode = input("What mode do you want to run?\n"
                 " 1 - force check for gif emojis\n"
                 " 2 - download as gif only if seen as gif\n"
                 " 3 - download all emojis as png (useful for non nitro plebs)\n")

folder_name = input(f"{smol_time()}How do you want to name the folder? "
                    f"Or press enter to leave the default name (today's date)\n")

if not folder_name.strip(' '):
    folder_name = today

if not os.path.exists(folder_name):
    os.makedirs(folder_name)


string = ""
try:
    with open("input") as input_file:
        string = input_file.read()
        if len(string) < 2:
            raise FileNotFoundError

except FileNotFoundError:
    print(
        "Enter the message html code that you want to strip emojis from \n"
        "To get said code you can either: \n"
        "- Press Ctrl+Shift+I on the discord app (but that will get the focus off the discord window and gif emojis "
        "will become pngs instead) or;\n"
        "- Open firefox (not sure if it exists on chrome), log into your discord account on it, select the "
        "message with the emojis and click on "
        '"View Selection Source": \n\nType "done" once you are done giving the source')
    string_input = ""

    while string_input.strip('"').lower() != "done":
        string_input = input()
        string = f"{string}{string_input}"

print(string)

start_time = time.time()


args = list()
i = 0

while i < len(string):
    if string[i:i + 4] == '<img':
        n = i + 4
        while string[n] != '>':
            n += 1
        if 'class="emoji' in string[i:n+1]:
            args.append(string[i:n + 1])
        print(string[i:n+1])
        i = n
    i += 1

for arg in args:
    split_str = arg.split(' ')
    emoji_link = str()
    emoji_name = str()
    emoji_type = str()
    emoji_real_type = str()

    for string in split_str:
        if "src" in string:
            emoji_link = string.replace('src="', '').replace('?v=1"', '')
            if "gif" in string:
                emoji_real_type = "gif"
            elif "png" in string:
                emoji_real_type = "png"
            elif "svg" in string:
                continue
        if "alt" in string:
            emoji_name = string.replace('alt=":', '').replace(':"', '')
    emoji_id = emoji_link.replace('https://cdn.discordapp.com/emojis/', '').replace('.' + emoji_real_type, '')

    if mode == "2":
        emoji_type = emoji_real_type
    elif mode == "1":
        emoji_type = "gif"
    elif mode == "3":
        emoji_type = "png"
    while emoji_id not in Emoji.ids and emoji_name in Emoji.names:
        try:
            emoji_name = emoji_name[:-1] + str(int(emoji_name[-1])+1)
        except ValueError:
            print(ValueError)
            emoji_name += "~1"
    if emoji_id not in Emoji.ids and not os.path.isfile(os.path.join(folder_name, emoji_name + "." + emoji_type)):
        Emoji(emoji_id, emoji_name, emoji_type)


class DownloaderThread(threading.Thread):
    threads = []

    def __init__(self, emojis):
        threading.Thread.__init__(self)
        self.downloaded = 0
        self.skipped = 0
        self.threadID = len(DownloaderThread.threads) + 1
        self.emojis = emojis
        self.img_data = b''
        DownloaderThread.threads.append(self)
        print(f"{smol_time()}Thread N°{self.threadID} created")

    @staticmethod
    def total_skipped():
        return sum(t.skipped for t in DownloaderThread.threads)

    @staticmethod
    def total_downloaded():
        return sum(t.downloaded for t in DownloaderThread.threads)

    @staticmethod
    def total_emojis_amount():
        return sum(len(t.emojis) for t in DownloaderThread.threads)

    @property
    def advancement(self):
        return self.downloaded / len(self.emojis)

    def run(self):
        print(f"{smol_time()}Thread N°{self.threadID} Initialized, {len(self.emojis)} assigned.")
        for emoji in self.emojis:
            print(f'{smol_time()}Thread ID {self.threadID} handling emoji ID {Emoji.emojis.index(emoji)}.')
            if emoji.already_downloaded():
                print(f'{smol_time()}"{emoji.name}" download skipped (already downloaded).')
                self.skipped += 1
                continue
            try:
                img_data = requests.get(emoji.link).content
            except RequestException:
                self.emojis.append(emoji)
                time.sleep(1)
                continue
            if img_data == b'' and emoji.type == "gif":
                emoji.type = "png"
                if emoji.already_downloaded():
                    print(f'{smol_time()}"{emoji.name}" download skipped (already downloaded).')
                    self.skipped += 1
                    continue
                try:
                    img_data = requests.get(emoji.link).content
                except RequestException:
                    self.emojis.append(emoji)
                    time.sleep(1)
                    continue
            with open(os.path.join(folder_name, emoji.name + "." + emoji.type), 'wb') as handler:
                handler.write(img_data)
                print(f'{smol_time()}"{emoji.name}" downloaded from {emoji.link}, {self.downloaded+1} '
                      f'downloaded out of {len(self.emojis)} emojis assigned for Thread {self.threadID}')
                self.downloaded += 1


amount_of_emojis = len(Emoji.emojis)

print(f'{smol_time()}{amount_of_emojis} emojis detected. Initializing download sequence...')

threads_to_run = amount_of_emojis
if threads_to_run > MAX_THREADS:
    threads_to_run = MAX_THREADS
emojis_per_thread = int(amount_of_emojis / threads_to_run)
beginning = 0

for i in range(threads_to_run):
    dl_thread = DownloaderThread(Emoji.emojis[beginning:beginning+emojis_per_thread])
    beginning += emojis_per_thread

while beginning < amount_of_emojis:
    DownloaderThread.threads[amount_of_emojis-beginning].emojis.append(Emoji.emojis[beginning])
    beginning += 1

for thread in DownloaderThread.threads:
    thread.start()

print(f'{smol_time()}All threads started!')

dled = DownloaderThread.total_downloaded
total = DownloaderThread.total_emojis_amount
skipped = DownloaderThread.total_skipped
time_since_last_print = 0

while True:
    loop_begin_time = time.time()
    time.sleep(THREADS_DONE_TIME_DELTA)
    all_threads_done = True
    for thread in DownloaderThread.threads:     # type: DownloaderThread
        if thread.is_alive():
            all_threads_done = False
    if all_threads_done:
        break
    time_since_last_print += time.time() - loop_begin_time
    if time_since_last_print >= 5:
        time_since_last_print = 0
        print(f"{smol_time()}___________________________________________________["
              f"{dled()} emojis downloaded out of {total()} ({dled()/total():.2%})...]")

# for emoji in Emoji.emojis:
#     print('Downloading "{0.name}" from {0.link} '.format(emoji))
#     img_data = requests.get(emoji.link).content
#
#     if img_data == b'':
#         emoji.type = "png"
#         if os.path.isfile(os.path.join(foder_name, emoji.name + "." + emoji.type)):
#             continue
#
#         img_data = requests.get(emoji.link).content
#
#     with open(os.path.join(folder_name, emoji.name + "." + emoji.type), 'wb') as handler:
#         handler.write(img_data)
#         i += 1
exec_time = time.time() - start_time
if skipped() > 0:
    print(f"{smol_time()}___________________________________________________\n"
          f"{smol_time()}___________________________________________________\n"
          f"{smol_time()}Download complete, {dled()} emojis downloaded and {skipped()} skipped in "
          f"{time.strftime('%Mm%Ss', time.localtime(exec_time))}{int((exec_time - int(exec_time))*1000)}ms!")
else:
    print(f"{smol_time()}___________________________________________________\n"
          f"{smol_time()}___________________________________________________\n"
          f"{smol_time()}Download complete, {dled()} emojis downloaded successfully in "
          f"{time.strftime('%Mm%Ss', time.localtime(exec_time))}{int((exec_time - int(exec_time))*1000)}ms!")
