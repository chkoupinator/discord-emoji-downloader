# Discord Emoji Downloader

A small Python script that can download emojis (both png and gif ones) from the html code of Discord messages.

This tool was mostly made for personal needs and the repo was made while I was still learning git and as such, it is quite messy. 

## Requirements & Installation
The only prerequisite for this is to install the [requests library](https://pypi.org/project/requests/) that handles HTTP requests.

To install it :

**Linux :** ```pip3 install requests```

**Windows / Mac :** ```pip install requests```

If you don't have python3 installed you can check [their website](https://www.python.org).

A version of python >= 3.5 is required although this was made using 3.6.

### Running it:
Simply use `python3 main.py` with a terminal open in the directory containing `main.py` if you're on Linux or `python main.py` if you're on Windows or Mac.

## Usage :

### Getting the HTML code of Discord messages:
1. Open the Discord desktop client or in your browser and press CTRL + SHIFT + I or the F12
2. Click on the inspector (cursor in a box in the top left corner of the dev tools usually)
3. Click on the message you want to extract emojis from or on the scroller at the right of the window to select all messages (make sure all messages are loaded).
4. Click on Copy -> Copy Element, and you're set.

> :warning: **Warning:** Do **not** share the code of your Discord client with others as it might contain your client token or sensitive messages! Proceed with caution! 

> Note: GIF emojis will become PNG emojis for performance reasons when you open the devs tools in the desktop client, you might wan to use your browser to get the HTML code if you want to download mixed gif/png emojis. You can also use the force gif checks option, but it might be slower. 

### Using the Downloader 
0. (Optional) put your HTML in the `input` file to make things faster for yourself, simply open it with a notepad and paste what you copied above. (Preferably erase it's contents once you're done)
1. Run it as explained in "Running it"
2. If you didn't put the code in the `input` file, paste it now and type `done` when you're done then press enter.
3. Select which download mode you want
4. (Optional) Give a name to the folder that will contain all the downloaded emojis, or press enter to use today's date.
